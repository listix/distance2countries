# Distance2Countries

This program colors the white pixels of an image according to which colors is the nearest. In order for this program to work you need to have the Pillow library(PIL should work as well):

```
$ pip3 install Pillow
```

The program allows you to see which country is the closest to any point but it works with any shape as long as a couple details are followed.
- The part that you want colored must always be pure white with no alpha (255,255,255,255)
- The country colors can be anything other than white and black with an alpha that is 255.
- If the countries are not touching the interested country you need to color their surroundings with black.
- There is no need to color the entire near countries. Coloring the closes border is enough.
- The oceans or anything you want to ignore should be totally transparent.

To call the program it requires two parameters, the type and the image.

```
./distance2country --type image
```

Type can be of 4 types:
--rand   This one uses a 10% of the white pixels of the image. It is the fastest and gives a quick idea of how the image would look.
--knn    Uses the K-nearest-neighbors algorithm. It is the third fastest but some noise on the divisions tends to happen.
--iter   This one checks each pixel against all the pixels in the border to find the nearest country. This is the bruteforce method and is the slowest.
--kdtree This uses KDTrees to store the boundary pixels, it makes finding the nearest much faster. This is the second fastest.

Both --iter and --kdtree will provide the same output but kdtree is the fastest so using that option is recommended.

### Examples
## Input
![Example output image](examples/boliviaInput.png)
## Using --rand
![Example output image](examples/rand.png)
## Using --knn
![Example output image](examples/knn.png)
## Using --iter
![Example output image](examples/iter.png)
## Using --kdtree
![Example output image](examples/kdtree.png)

## Hungary
# Input
As long as the border is colored it doesnt need to be perfect.
![Example output image](examples/hungaryInput.png)
# Output
![Example output image](examples/hungary.png)

## Mexico
# Input
Notice that the thickness of the border is not important.
![Example output image](examples/mexicoInput.png)
# Output
![Example output image](examples/mexico.png)

