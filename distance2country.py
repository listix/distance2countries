#!/usr/bin/python3

import sys
import scipy.spatial as spatial
import math
import random
from PIL import Image

WHITE = (255,255,255,255)
BLACK = (0,0,0,255)
TRANSPARENT = (0,0,0,0)

LANDMASS = [BLACK,WHITE]

def truncate(number,decimals=0):
    power = 10**decimals
    return math.trunc(number*power)/power

def distance(point1,point2):
    return math.sqrt((point1[0]-point2[0])**2+(point1[1]-point2[1])**2)

def numberToCoordinates(number,size):
    width,height = size
    return (number % width,int(number/width))

def coordinateToNumber(coordinates,size):
    width,height = size
    return coordinates[0] + coordinates[1]*width

def getSurroundingPositions(position,size):
    width, height = size
    x, y = position
    result = [(x+i,y+j) for i in [-1,0,1] for j in [-1,0,1] if (i + j) != 0]
    return [pos for pos in result if 0<=pos[0] < width and 0<=pos[1] < height]

def isBorder(imgdata,position,size):
    borderingpixels = getSurroundingPositions(position,size)
    return any([imgdata[coordinateToNumber(bp,size)] in LANDMASS for bp in borderingpixels])

def getCountryBorderPixels(imgdata,size):
    borderpixels = []
    countrypixels = []
    countriescolors = []
    for pixelidx in range(len(imgdata)):
        if imgdata[pixelidx] not in countriescolors and imgdata[pixelidx] not in [WHITE,BLACK,TRANSPARENT] and imgdata[pixelidx][3] == 255:
            countriescolors.append(imgdata[pixelidx])
        if imgdata[pixelidx] in countriescolors:
            if isBorder(imgdata,numberToCoordinates(pixelidx,size),size):
                borderpixels.append(numberToCoordinates(pixelidx,size))
        elif imgdata[pixelidx] == WHITE:
            countrypixels.append(numberToCoordinates(pixelidx,size))
    return [borderpixels,countrypixels]

def predictClasificationKDTree(tree,pixel,k,data,coloredpixels,size):
    distance,neighborsindexes = tree.query(pixel,k)
    neighbors = [data[i] for i in neighborsindexes]
    outputvalues = [coloredpixels[coordinateToNumber(pixel,size)] for pixel in neighbors] 
    if len(outputvalues) > 0:
        colorprediction = max(set(outputvalues), key=outputvalues.count)
    else:
        print([point,outputvalues])
        colorprediction = BLACK
    return colorprediction

def KDTreePointDistance(imagename):
    img = Image.open(imagename)
    imgdata = list(img.getdata())
    size = img.size

    borderpixels,countrypixels = getCountryBorderPixels(imgdata,size)
    tree = spatial.KDTree(borderpixels)

    newimg = Image.new("RGBA",size,TRANSPARENT)
    newpixels = list(newimg.getdata())

    counter = 0
    for countrypixel in countrypixels:
        counter += 1
        bestdist,idx = tree.query(countrypixel,k=1)
        color = imgdata[coordinateToNumber(borderpixels[idx],size)]
        newpixels[coordinateToNumber(countrypixel,size)] = color
        if (counter % 1000) == 0:
            print(f"{100*counter/len(countrypixels):.2f}" + '% Complete')

    newimg.putdata(newpixels)
    newimg.show()
    newimg.save('kdtree_distance_' + imagename)

def iterativePointDistance(imagename):
    img = Image.open(imagename)
    imgdata = list(img.getdata())
    size = img.size

    newimg = Image.new("RGBA",size,TRANSPARENT)
    newpixels = list(newimg.getdata())

    borderpixels,countrypixels = getCountryBorderPixels(imgdata,size)
    #borderpixels = random.sample(borderpixels,int(len(borderpixels)/10))
    counter =0
    for countrypixel in countrypixels:
        counter += 1
        dist = 0
        bestdist = 100000
        color = None
        for borderpixel in borderpixels:
            dist = distance(countrypixel,borderpixel)
            if dist < bestdist:
                bestdist = dist
                color = imgdata[coordinateToNumber(borderpixel,size)]
        newpixels[coordinateToNumber(countrypixel,size)] = color
        if (counter % 1000) == 0:
            print(f"{100*counter/len(countrypixels):.2f}" + '% Complete')

    newimg.putdata(newpixels)
    newimg.show()
    newimg.save('iterative_distance_' + imagename)

def randomPointDistance(imagename):
    img = Image.open(imagename)
    imgdata = list(img.getdata())
    size = img.size


    newimg = Image.new("RGBA",size,TRANSPARENT)
    newpixels = list(newimg.getdata())

    borderpixels,countrypixels = getCountryBorderPixels(imgdata,size)
    borderpixels = random.sample(borderpixels,int(len(borderpixels)/10))
    tree = spatial.KDTree(borderpixels)
    samplecountrypixels = random.sample(countrypixels,int(len(countrypixels)/10))

    counter = 0
    for countrypixel in samplecountrypixels:
        counter += 1
        bestdist = 100000
        bestdist,idx = tree.query(countrypixel,k=1)
        color = imgdata[coordinateToNumber(borderpixels[idx],size)]
        newpixels[coordinateToNumber(countrypixel,size)] = color
        if (counter % 1000) == 0:
            print(f"{100*counter/len(samplecountrypixels):.2f}" + '% Complete')
    newimg.putdata(newpixels)
    newimg.show()
    newimg.save('random_distance_' + imagename)

def KNNPointDistance(imagename):
    img = Image.open(imagename)
    imgdata = list(img.getdata())
    size = img.size

    newimg = Image.new("RGBA",size,TRANSPARENT)
    newpixels = list(newimg.getdata())

    borderpixels,countrypixels = getCountryBorderPixels(imgdata,size)
    #borderpixels = random.sample(borderpixels,int(len(borderpixels)/10))
    samplecountrypixels = random.sample(countrypixels,int(len(countrypixels)/10))

    counter = 0
    for countrypixel in samplecountrypixels:
        counter += 1
        dist = 0
        bestdist = 100000
        color = None
        for borderpixel in borderpixels:
            dist = distance(countrypixel,borderpixel)
            if dist < bestdist:
                bestdist = dist
                color = imgdata[coordinateToNumber(borderpixel,size)]
        newpixels[coordinateToNumber(countrypixel,size)] = color
        if (counter % 1000) == 0:
            print(f"{100*counter/len(countrypixels):.2f}" + '% Complete')

    tree = spatial.KDTree(samplecountrypixels)
    
    counter =0 
    for pixel in countrypixels:
        counter += 1
        if (counter % 1000) == 0:
            print(f"{100*counter/len(countrypixels):.2f}" + '% Complete')
        newpixels[coordinateToNumber(pixel,size)] = predictClasificationKDTree(tree,pixel,30,samplecountrypixels,newpixels,size)
    newimg.putdata(newpixels)
    newimg.show()
    newimg.save('knn_distance_' + imagename)


#randomPointDistance('bolivia3Bresized.png') #00:07.74
#KDTreePointDistance('bolivia3Bresized.png') #2:15
#iterativePointDistance('bolivia3Bresized.png') #7:29
#KNNPointDistance('bolivia3Bresized.png') #3:32

def main(args):
    '''Check received parameters and select which type of program we are going to run.'''
    if len(args) < 2:
        print('Usage:./appcolorsort Type File1')
        print('Program needs a type and the image file with the borders colored.')
        print('--rand is the fastest but least accurate')
        print('--knn is the third fastest but produces some noise')
        print('--iter takes the longest to run but is accurate')
        print('--kdtree is the second fastest but produces accurate output.(Reccommended)')
    else:
        programtype = args[0]
        if programtype == '--rand':
            randomPointDistance(args[1])
        elif programtype == '--knn':
            KNNPointDistance(args[1])
        elif programtype == '--iter':
            iterativePointDistance(args[1])
        elif programtype == '--kdtree':
            KDTreePointDistance(args[1])

main((sys.argv)[1:])
